"use strict";

$(document).ready(function () {
    
    var screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
    if (screen_width <= 992) {
        // mobile btn hide/show
        $('.mobile--collapse').click(function () {
            $(this).hide('1000').parent().find('.mobile--collapse__desc').show('500');
        });
        $('.itemFull').click(function () {
            $(this).parent().toggleClass('active');
        });
        $('.dropdown').click(function () {
            $(this).toggleClass('active');
        });
        $('.filterShow').click(function () {
            $(this).parent().hide().next().show('slow').addClass('active').find('.textFilter').addClass('active').find('.hideFilter').hide().parent().find('.showFilter').show();
        });
        $('.hide--js').click(function () {
            var id  = $(this).attr('href');
            $(this).parents().find('.filterShow').parent().show().parents().find('.filter').hide();
            
            setTimeout(function(){ 
                var top = $('.mainSales').offset().top;
                $('body,html').animate({scrollTop: top - 80 }, 500);
             }, 100);
            
            return false;
        });
        // mobile btn hide/show
    }
    

    CloudZoom.quickStart();

    // mask phone
//    $("[name=phone],[name=RecallForm\\[phone\\]]").mask(phoneMask);
    // mask phone

    $(function () {
        /*$(".cardColor__select-js1").change(function () {
            $('[class ^= mat]').not($($(this).find('option:selected').attr('data-value')).addClass('active')).removeClass('active');
        });
        $(".cardColor__select-js2").change(function () {
            $('[class ^= col]').not($($(this).find('option:selected').attr('data-value')).addClass('active')).removeClass('active');
        });
        $(".cardColor__select-js3").change(function () {
            $('[class ^= onl]').not($($(this).find('option:selected').attr('data-value')).addClass('active')).removeClass('active');
        });*/
    });

    $.rangeSliderMini = function (range, start, end, min, max) {
            var step = (max - min)/100;
            noUiSlider.create(range, {
                start: [start.val(), end.val()],
                connect: true,
                step: step,
                range: {
                    'min': min,
                    'max': max
                },
                pips: {
                    mode: 'positions',
                    values: [0, 50, 100],
                    density: 100
                }
            });
            range.noUiSlider.on('update', function (values, handle) {
                var value = values[handle];

                var start = $(this.target).eq(0).closest('.filter__item-range').find('input.start');
                var end = $(this.target).closest('.filter__item-range').find('input.end');
                if (handle) {
                    end.val(Math.round(value));
                } else {
                    start.val(Math.round(value));
                }
            });
            range.noUiSlider.on('end', function (values, handle) {
                var value = values[handle];

                var start = $(this.target).eq(0).closest('.filter__item-range').find('input.start');
                var end = $(this.target).closest('.filter__item-range').find('input.end');
                if (handle) {
                    end.trigger('change');
                } else {
                    start.trigger('change');
                }
            });
            end.on('change', function () {
                range.noUiSlider.set([null, this.value]);
            });
            start.on('change', function () {
                range.noUiSlider.set([this.value, null]);
            });
        };

    // range slider
    if ($("#rangeSlider").length) {
        var range1 = document.getElementById('rangeSlider'),
            range2 = document.getElementById('rangeSlider1'),
            range3 = document.getElementById('rangeSlider2'),
            range4 = document.getElementById('rangeSlider3'),
            range5 = document.getElementById('rangeSlider4'),
            range6 = document.getElementById('rangeSlider5');
        var start1 = document.getElementById('start'),
            start2 = document.getElementById('start1'),
            start3 = document.getElementById('start2'),
            start4 = document.getElementById('start3'),
            start5 = document.getElementById('start4'),
            start6 = document.getElementById('start5');
        var end1 = document.getElementById('end'),
            end2 = document.getElementById('end1'),
            end3 = document.getElementById('end2'),
            end4 = document.getElementById('end3'),
            end5 = document.getElementById('end4'),
            end6 = document.getElementById('end5');

        $.rangeSlider = function (range, start, end) {
            noUiSlider.create(range, {
                start: [301237890, 890000890],
                connect: true,
                step: 10000,
                range: {
                    'min': 123890,
                    'max': 890000890
                },
                pips: {
                    mode: 'positions',
                    values: [0, 50, 100],
                    density: 100
                }
            });
            range.noUiSlider.on('update', function (values, handle) {
                var value = values[handle];
                if (handle) {
                    end.value = Math.round(value);
                } else {
                    start.value = Math.round(value);
                }
            });
            end.addEventListener('change', function () {
                range.noUiSlider.set([this.value, null]);
            });
            start.addEventListener('change', function () {
                range.noUiSlider.set([null, this.value]);
            });
        };

        $.rangeSlider(range1, start1, end1);
        $.rangeSliderMini(range2, start2, end2);
        $.rangeSliderMini(range3, start3, end3);
        $.rangeSliderMini(range4, start4, end4);
        $.rangeSliderMini(range5, start5, end5);
        $.rangeSliderMini(range6, start6, end6);

    }
    $('.rangeSlider').each(function(){
            var start = $(this).closest('.filter__item-range').find('input.start');
            var end = $(this).closest('.filter__item-range').find('input.end');
            var min = parseFloat($(this).closest('.filter__item-range').find('input.start').attr('data-min'));
            var max = parseFloat($(this).closest('.filter__item-range').find('input.end').attr('data-max'));
            if(max <= min)
                max = max+1;

            $.rangeSliderMini(this, start, end, min, max);
        });
    // range slider

    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    // carousel slider
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 0,
        items: 1,
        nav: true,
        navText: ["<img src='images/prevArr.png'>", "<img src='images/nextArr.png'>"],
        dots: false
    });
    // carousel slider

    //// owl slider
    var $sync1 = $(".big-images"),
        $sync2 = $(".thumbs"),
        flag = false,
        duration = 300;
    $(".big-images").each(function() {
        if($(this).find('.item').length > 1)
        {
            $(this).owlCarousel({
                items: 1,
                margin: 10,
                nav: false,
                loop: false,
                dots: false
            }).on('changed.owl.carousel', function (e) {
                if (!flag) {
                    flag = true;
                    $(this).closest('.cardSlider').find(".thumbs").trigger('to.owl.carousel', [e.item.index, duration, true]);
                    flag = false;
                }
            });
        }
    });
    $(".thumbs").each(function() {
        if($(this).find('.item').length > 1)
        {
            $(this).owlCarousel({
                margin: 2,
                items: 6,
                nav: true,
                navText: ["<img src='/web/images/prevArr.png'>", "<img src='/web/images/nextArr.png'>"],
                center: false,
                dots: false,
                //loop: true,
                mouseDrag: false
            }).on('click', '.owl-item', function () {
                $(this).closest('.cardSlider').find(".big-images").trigger('to.owl.carousel', [$(this).index(), duration, true]);
            }).on('changed.owl.carousel', function (e) {
                if (!flag) {
                    flag = true;
                    $(this).closest('.cardSlider').find(".big-images").trigger('to.owl.carousel', [e.item.index, duration, true]);
                    flag = false;
                }
            });
        }
    });
    $(function () {
        $('.thumbs').on('click', '.owl-item', function () {
            $(this).addClass('main').siblings().removeClass('main');
            //return false;
        });
    });
    // owl slider

    //filter count
    /*$('.filter__item input').on('change', function (e) {
        $('#count').text('Подобрано ' + ($(':checkbox:checked').length * 129 + Math.round($('#start').val() * 0.0001)) + ' моделей');
        $('.filter-count-js').show();
        $('.textFilterLeft').show();
        var x = $(this).parent().offset();
        //console.log($('#count'));
        //console.log($('#end').val());
        $(".filter-count-js").css({
            "top": x.top - 8,
            "left": x.left + 170
        });
        setTimeout(function(){ $('.filter-count-js').removeClass('filter-right').hide(); }, 3000);
    });
    $('.filter__item:last-child input').on('change', function (e) {
        $('.filter-count-js').addClass('filter-right');
        var x = $(this).parent().offset();
        $(".filter-count-js").css({
            "left": x.left - 300
        });
    });*/
    //filter count

    // count input
    $(document).on('click', '.input__quantity-js .inc', function () {
        var $input = $(this).parents('.input__quantity-js').find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        //$input.change();
        $input.trigger('change');
        return false;
    });
    $(document).on('click', '.input__quantity-js .dec', function () {
        var $input = $(this).parents('.input__quantity-js').find('input');
        var count = parseInt($input.val()) + 1;
        count = count > 999 ? 1 : count;
        $input.val(count);
        //$input.change();
        $input.trigger('change');
        return false;
    });
    // count input

    // filter
    $('.show--js').click(function () {
        $(this).parents().find('.filter').addClass('active').find('.filter__inner').addClass('active').parents().find('.textFilter').addClass('active');
        return false;
    });
    $('.hide--js').click(function () {
        var id  = $(this).attr('href');

        $(this).parents().find('.filter').removeClass('active').find('.filter__inner').removeClass('active').parents().find('.textFilter').removeClass('active');
		if($(id).length)
		{
			var top = $(id).offset().top;
			$('body,html').animate({scrollTop: top }, 500);
		}
        return false;
    });
    // filter

    //busket delete function
    /*$('.busket__delete-js a').click(function () {
        $(this).parent().parent().fadeOut('slow');
        return false;
    });*/




    //selectOrDie
    // AND
    // linkedselect
    if ($("select").length) {
        $('select.list').selectOrDie({
            onChange: function onChange() {
                var syncList1 = new syncList();
//                syncList1.dataList = {
//                    'list1': {
//                        'list1_of1': 'Боровляны',
//                        'list2_of1': 'Борисов'
//                    },
//                    'list2': {
//                        'list2_of1': 'Борисов'
//                    },
//                    'list3': {
//                        'list3_of1': 'Минск'
//                    },
//                    'list1_of1': {
//                        'list4_of1': 'г. Минск. Магазин «Мебель Пинскдрев», (мебельный центр «Озерцо»), Меньковский тракт, 43 (напротив авторынка «Малиновка»)'
//                    },
//                    'list2_of1': {
//                        'list5_of1': 'Минский р-н, д. Боровляны. Магазин «Мебель Пинскдрев», ул. 40 лет Победы, 25Б'
//                    },
//                    'list3_of1': {
//                        'list6_of1': 'г. Борисов. Магазин «Пинскдрев», ул. Гагарина, 111'
//                    }
//                };
//                syncList1.sync("List1", "List2", "List3");
//                $("select.list").selectOrDie("update");
                syncList1.dataList = pointDataList;
                syncList1.sync("List2", "List3");
                $("select.list").selectOrDie("update");
                updateMapCenter();
            }
        });
        $('select').selectOrDie();
    }

    function updateMapCenter()
    {
      var val = $('select#List3').val();
      map.setCenter([pointCoords[val][0], pointCoords[val][1]], 12);
      var placemark = new ymaps.Placemark([pointCoords[val][0], pointCoords[val][1]]);
      map.geoObjects.add(placemark);
      //pointWorkTime
      $('div#worktime-block').html(pointWorkTime[val]);
      $('p#point-phone').html(pointPhone[val]);
    }


    $('select#List3').change(function(){
      updateMapCenter();
    })

    // fancybox
    $(".fancybox-thumb").fancybox({
        width       : '70%',
        height      : '70%',
        helpers: {
            title: {
                type: 'outside'
            },
            thumbs: {
                width: 70,
                height: 50
            }
        }
    });

    // fancybox
    function addFancyBox()
    {
      var images = $("img.preview");
      for(var i in images)
      {
        if(isNaN(parseInt(i)))
        {
          continue;
        }
        var html = images[i].outerHTML;
        var link = images.eq(i).attr('src');
        images.eq(i).replaceWith('<a class="fancybox-preview" href="'+link+'">'+html+'</a>');
      }
      $(".fancybox-preview").fancybox();
    }
    addFancyBox();


    // order tabs
    $('input[name=Order\\[delivery_type\\]]').click(function () {
        var v = $(this).val();
        console.log(v);
        if (v == 0) {
            $('.order3').show();
            $('.order4').hide();
        } else if (v == 1) {
            $('.order3').hide();
            $('.order4').show();
        }
    });
    // order tabs

	var $sync3 = $(".newsSlider .big-images"),
        $sync4 = $(".newsSlider .thumbs");
    $sync3.owlCarousel({
        items: 1,
        margin: 10,
        nav: false,
        loop: true,
        dots: false
    }).on('changed.owl.carousel', function (e) {
        if (!flag) {
            flag = true;
            $sync4.trigger('to.owl.carousel', [e.item.index, duration, true]);
            flag = false;
        }
    });
    $sync4.owlCarousel({
        margin: 2,
        items: 11,
        nav: true,
        navText: ["<img src='images/prevArr.png'>", "<img src='images/nextArr.png'>"],
        center: false,
        dots: false,
        //loop: true,
        mouseDrag: false
    }).on('click', '.owl-item', function () {
        $sync3.trigger('to.owl.carousel', [$(this).index(), duration, true]);
    }).on('changed.owl.carousel', function (e) {
        if (!flag) {
            flag = true;
            $sync3.trigger('to.owl.carousel', [e.item.index, duration, true]);
            flag = false;
        }
    });
    $(function () {
        $('.thumbs').on('click', '.owl-item', function () {
            $(this).addClass('main').siblings().removeClass('main');
            //return false;
        });
    });

      if(cardCounter !== undefined && cardCounter > 0)
      {
        $('#cardCounter').html('('+cardCounter+')');
      }


});

$(window).scroll(function () {
    if ($(this).scrollTop() > 0) {
        $('#scroll').fadeIn();
    } else $('#scroll').fadeOut();
});
$('#scroll').click(function () {
    $('body,html').animate({ scrollTop: 0 }, 400);
    return false;
});


if ($("#menuSales").length) {
        $(function(){
             var topPos = $('.menuSales').offset().top;
             $(window).scroll(function() {
                  var top = $(document).scrollTop();
                  if (top > topPos) $('.menuSales').addClass('fixed');
                  else $('.menuSales').removeClass('fixed');
             });
        });
}

//init csrf
//$('input[name=_csrf]').val($('meta[name=csrf-token]').attr('content'));

function recallForm(form)
{
  try{
                $('input,textarea').tooltip('destroy');
            $('.reCaptcha-error-form').tooltip('destroy');
    $.ajax(
      {
          url: $(form).attr('action'),
          cache: false,
          type: 'POST',
          dataType: 'json',
          data: $(form).serialize(),
          success: function(data){
            $('input,textarea').addClass('has-success').removeClass('has-error');
            if(data !== undefined && data.error === false)
            {
              $("#call-id").modal("hide");
              $("#call-thank").modal("show");
              return false;
            }
            if(data !== undefined && data.error === true)
            {
              for(var i in data.errorlist)
              {
                if(i == 'reCaptcha')
                {
                  $('.reCaptcha-error-form').tooltip({
                                      trigger: 'manual',
                                      placement: 'right',
                                      title: data.errorlist[i]
                                  }).tooltip('show');
                }
                else
                {
                  $('input[name=RecallForm\\['+i+'\\]]').parents('.form-group').addClass('has-error').removeClass('has-success');
                               $('[name=RecallForm\\['+i+'\\]]').tooltip({
                                      trigger: 'manual',
                                      placement: 'right',
                                      title: data.errorlist[i]
                                  }).tooltip('show');
                }
              }
              return false;
            }
          }
        })
  }
  catch(e)
  {
    console.warn(e);
  }
  return false;
}

function setCity(id)
{
  try{
    $.ajax(
      {
          url: '/site/city/',
          cache: false,
          type: 'POST',
          dataType: 'json',
          data: "id="+id+"&_csrf="+$('[name=csrf-token]').attr('content'),
          success: function(data){
            console.log(data);
            location.reload();
          }
        })
  }
  catch(e)
  {
    console.warn(e);
  }
  return false;
}

function setCookie(name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  }

  document.cookie = updatedCookie;
}
function tabclickRegister()
    {
      $('#tab3-click').click(function(){$('a[href=\\#tab3]').click();return false;});
      $('#tab4-click').click(function(){$('a[href=\\#tab4]').click();return false;});
      $('#tab5-click').click(function(){$('a[href=\\#tab5]').click();return false;});
    }
    tabclickRegister();

function scrollTo(elem)
{
    if($(elem).length)
        $("html, body").animate({ scrollTop: $(elem).position().top - 100 }, 500);
}

//инициализация автозаполнения поиска
function initSearchForm(data)
{
    var options = {
        data: data,
        getValue: "name",
        list: {
            match: {
                enabled: true
            }
        },
        template: {
            type: "custom",
            method: function method(value, item) {
                //return "<span class='flag flag-" + (item.img).toLowerCase() + "' ></span>" + value;
                return "<div class='row'><div class='pull-left'>" + (item.img != false ? item.img : "" )+ "</div>" + "<a href='" + item.url + "' class='text--md'>" + value + "</a></div>";
            }
        }
    };
    $("#search").easyAutocomplete(options);
    $("input#search").click();
    // autocomplete
}

$(window).load(function () {
  //получение данных для автозаполнения поиска
        $.ajax(
          {
            type: "GET",
            url: "/site/productsearch/",
            success: initSearchForm,
            dataType: 'json'
          }
          );
})